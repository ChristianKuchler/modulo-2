from db import db

class modulo(db.Model):
      __tablename__='modulo'
      id=db.Column(db.Integer, primary_key=True)
      module_name=db.Column(db.String(40))
      level=db.Column(db.Integer)
      description=db.Column(db.String(80))


      def __init__(self,module_name,level,description):
          self.module_name
          self.level
          self.description

      def save_to_db(self):
          db.session.add(self)
          db.session.commit()

      def delete_from_db(self):
          db.session.delete(self)
          db.session.commit()    

      @classmethod
      def find_by_name(cls,module_name):
          return cls.query.filterby(module_name=module_name).first()


