from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Api
from module.resource import(
    create_module,
)

app=Flask(__name__)
app.config['DEBUG']= True
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///data.db' 
db = SQLAlchemy(app)

@app.before_first_request
def create_db():
    db.create_all()

api = Api(app)
api.add_resource(create_module, '/create/module')


if __name__=='__main__':
    from db import db
    db.init_app(app)
    app.run