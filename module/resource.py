from flask_restful import Resource,reqparse
from models.data_model import modulo
module_parser= reqparse.RequestParser()


module_parser.add_argument(  'module_name',
                            type=str,
                            required=True,
                            help="This field cannot be blank."
                            )
module_parser.add_argument(  'level',
                            type=str,
                            required=True,
                            help="This field cannot be blank."
                            )
module_parser.add_argument(  'Description',
                            type=str,
                            required=True,
                            help="This field cannot be blank."
                            )  


class create_module(Resource):
    def post(self):
        data = module_parser.parse_args() 
       
        if modulo.find_by_name(data["module_name"]):
            return {"message": "This module already exists ..."}

        modulo_ident = modulo(data['module_name'],
         data['level'],
         data['description'])
        modulo_ident.save_to_db()
        return {"message": "Module has been created..."}
